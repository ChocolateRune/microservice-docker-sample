// load mongoose
const mongoose = require("mongoose");
const OrderModel = mongoose.Schema({
    CustomerID: {
        type: mongoose.ObjectId,
        require: true
    },
    BookID: {
        type: mongoose.ObjectId,
        require: true
    },
    initialDate: {
        type: Date,
        require: true
    },
    deliveryDate: {
        type: Date,
        require: true
    }
}, { versionKey:false , collection: 'Order'});


module.exports = mongoose.model("Order", OrderModel);