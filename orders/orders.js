//get ip
var ip = require("ip");
console.dir ( ip.address() );

// check global env
if(process.env.CUSTOMER_URL) { 
    CUSTOMER_URL = process.env.CUSTOMER_URL;
    console.log('CUSTOMER_URL is running on Container! : ' + CUSTOMER_URL); 
}
else { 
    CUSTOMER_URL = 'http://localhost:5555/customer/';
    console.log('CUSTOMER_URL is running on Develop! : ' + CUSTOMER_URL); 
}

if(process.env.BOOK_URL) { 
    BOOK_URL = process.env.BOOK_URL;
    console.log('BOOK_URL is running on Container! : ' + BOOK_URL); 
}
else { 
    BOOK_URL = 'http://localhost:4545/book/';
    console.log('BOOK_URL is running on Develop! : ' + BOOK_URL); 
}


// let CUSTOMER_URL = 'http://localhost:5555/customer/',
//     BOOK_URL = 'http://localhost:4545/book/';

// load express
// console.log(CUSTOMER_URL);
// console.log(BOOK_URL);
require('./db/mongoose');
    const express = require("express");
    const app = express();
    const axios = require("axios");
    const bodyParser = require("body-parser");

    app.use(bodyParser.json());
    const Order = require('./Order');
    const { mongo } = require('mongoose');
    const mongoose = require('./db/mongoose');

app.post("/order", (req, res) => {

        console.log('Order Create Detected');
        var newOrder = {
            CustomerID: mongoose.Types.ObjectId(req.body.CustomerID),
            BookID: mongoose.Types.ObjectId(req.body.BookID),
            initialDate: req.body.initialDate,
            deliveryDate: req.body.deliveryDate
        }

        var order = new Order(newOrder)

        order.save().then(() => {
            res.send("Order created with success!");
            console.log("Order created with success!");
        }).catch((err) => {
            throw err;
        })
        
})

app.get("/order", (req, res) => {
        Order.find().then((books) => {
            res.json(books)
        }).catch((err) => {
            throw err;
        })
})

app.get("/order/:id", (req, res) => {
    Order.findById(req.params.id).then((order) => {
        if(order){
            axios.get(CUSTOMER_URL + order.CustomerID).then((response) => {
                var orderObject = {customerName: response.data.name, bookTitle: ''}
                axios.get(BOOK_URL + order.BookID).then((response) => {
                    orderObject.bookTitle = response.data.title
                    res.json(orderObject)
                })
                console.log(response)
                
            })
           
        }else{
            res.send("Invalid Order")
        }
    })
})


app.listen("7777", () => {
    console.log("Up and Running! --- Order service");
})