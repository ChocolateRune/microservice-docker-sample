// load express
require('./db/mongoose');
    const express = require("express");
    const app = express();
    const bodyParser = require("body-parser");

    app.use(bodyParser.json());
    const Customer = require('./Customer');

    app.post("/customer", (req, res) => {
        console.log('Customer Create Detected');
        var newCustomer = {
            name: req.body.name,
            age: req.body.age,
            address: req.body.address
        }

        var customer = new Customer(newCustomer)

        customer.save().then(() => {
            res.send("Customer created with success!")
        }).catch((err) => {
            throw err;
        })
    })

    app.get("/customers", (req, res) => {
        Customer.find().then((customers) => {
            res.json(customers)
        }).catch((err) => {
            throw err;
        })
    })

    app.get("/customer/:id", (req, res) => {
        Customer.findById(req.params.id).then((customer) => {
            if(customer){
                res.json(customer)
            }else{
                res.send("Invalid ID")
            }
        }).catch((err) => {
            if(err){
                throw err;
            }
        })
    })

    app.delete("/customer/:id", (req, res) => {
        Customer.findByIdAndDelete(req.params.id).then(() => {
            res.send("Customer deleted with success!")
        }).catch((err) => {
            if(err){
                throw err;
            }
        })
    })

app.listen("5555", () => {
    console.log("Up and Running! --- Customers service");
})