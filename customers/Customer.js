// load mongoose
const mongoose = require("mongoose");
const CustomerModel = mongoose.Schema({
    name: {
        type: String,
        require: true
    },
    age: {
        type: Number,
        require: true
    },
    address: {
        type: String,
        require: true
    }
}, { versionKey:false , collection: 'Customer'});


module.exports = mongoose.model("Customer", CustomerModel);