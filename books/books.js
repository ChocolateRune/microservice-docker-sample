// load express
require('./db/mongoose');
    const express = require("express");
    const app = express();
    const bodyParser = require("body-parser");

    app.use(bodyParser.json());
    const Book = require('./Book');

// load mongoose -> move to ./db/mongoose.js

    //connect
    // mongoose.connect("mongodb://127.0.0.1:27017/booksservice", () => {
    //     console.log("Database is connected!");
    // });

app.get('/', (req, res) => {
    res.send("This is our main endpoint!");
})

//Create Function
    app.post("/book", (req, res) => {
        console.log('New Book Create Detected');
        // This our create function
        var newBook = {
            title: req.body.title,
            author: req.body.author,
            numberPages: req.body.numberPages,
            publisher: req.body.publisher
        }

        var book = new Book(newBook)

        book.save().then(() => {
            console.log("New book created with success!")
            res.json(book)
        }).catch((err) => {
            if(err){
                throw err;
            }
        })
    })

    app.get("/books", (req, res) => {

        Book.find().then((books) => {
            res.json(books)
        }).catch(err => {
            if(err){
                throw err;
            }

        })
    })

    app.get("/book/:id", (req, res) => {
        Book.findById(req.params.id).then((book) => {

            if(book){
                //Book Data
                res.json(book)
            }else{
                res.sendStatus(404);
            }

        }).catch(err => {
            if(err){
                throw err;
            }
        })
    })

    app.delete("/book/:id", (req, res) => {
        Book.findOneAndRemove(req.params.id).then(() => {
            res.send("Book Removed with Success!")
        }).catch(err => {
            throw err;
        })
    })

app.listen(4545, () => {
    console.log("Up and Running! --- This is our Books service");
})