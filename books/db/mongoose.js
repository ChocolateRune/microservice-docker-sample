const mongoose = require("mongoose");

//connect
mongoose.connect(process.env.MONGO_URL).catch((err) => {
    if(err){
        console.log('Trying to Connect : mongodb://127.0.0.1:27017/booksservice');
        mongoose.connect("mongodb://127.0.0.1:27017/booksservice");
    } else {
        db.on('error', (err)=>{
            console.log('PLEASE-CHECK-DATABASE');
        })
    }
})

const db = mongoose.connection;

db.once('open', function callback() {
    console.log('DB-Connected');
})

module.exports = mongoose;