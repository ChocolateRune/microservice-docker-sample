// load mongoose
const mongoose = require("mongoose");
const BookModel = mongoose.Schema({
    title: {
        type: String,
        require: true
    },
    author: {
        type: String,
        require: true
    },
    numberPages: {
        type: Number,
        require: true
    },
    publisher: {
        type: String,
        require: true
    }
}, { versionKey:false , collection: 'Book'});


module.exports = mongoose.model("Book", BookModel);